from rest_framework import serializers
from .models import Transaction,AlprGroup,SystemHeartBeat,CameraHeartBeat,VehicleMaster,ANPRLPDetails,ANPRMaster,ANPRVehicleRegisterationDetail,Image

class AlprSerializer(serializers.ModelSerializer):
    class Meta:
        model=AlprGroup
        fields="__all__"

class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model=Transaction
        fields="__all__"



class SystemHeartBeatSerializer(serializers.ModelSerializer):
    
    class Meta:
        model=SystemHeartBeat
        fields="__all__"
        depth=1


class CameraHeartBeatSerializer(serializers.ModelSerializer):
    class Meta:
        model =CameraHeartBeat
        fields = "__all__"

class VehicleMasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleMaster
        fields = "__all__" 

		
class ANPRVehicleRegisterationDetailSerializer(serializers.ModelSerializer):
	class Meta:
		model=ANPRVehicleRegisterationDetail
		fields = "__all__"
		
class ANPRMasterSerializer(serializers.ModelSerializer):
	class Meta:
		model=ANPRMaster
		fields="__all__"
class ANPRLPDetailsSerializer(serializers.ModelSerializer):
	class Meta:
		model=ANPRLPDetails
		fields = "__all__"

class ImageSerializer(serializers.ModelSerializer):
	class Meta:
		model=Image
		fields = "__all__"        