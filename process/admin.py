from django.contrib import admin
from .models import Transaction,Image,ANPRLPDetails,ANPRMaster,ANPRVehicleRegisterationDetail
# Register your models here.
admin.site.register([Transaction,Image,ANPRLPDetails,ANPRMaster,ANPRVehicleRegisterationDetail])