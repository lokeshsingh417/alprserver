from django.db import models
import uuid

# Create your models here.


class Image(models.Model):
    image=models.ImageField(upload_to="frame/image")

class VehicleMaster(models.Model):
    master_id = models.UUIDField(primary_key=True,editable=False,default = uuid.uuid4)
    number_plate = models.CharField(max_length=200,null=True,blank = True)
    name = models.CharField(max_length=200,null=True,blank = True,default="NA")
    confidence =  models.CharField(max_length=200,null=True,blank = True)
    camera_id =  models.CharField(max_length=200,null=True,blank = True)
    label = models.CharField(max_length=200,null=True,blank = True,default="NA")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return str(self.number_plate)




class CameraHeartBeat(models.Model):   
    camera_hearbeat_id = models.UUIDField(primary_key=True,editable=False,default = uuid.uuid4)
    camera_id = models.CharField(max_length=200,null=True,blank = True)
    camera_name=models.CharField(max_length=200,null=True,blank = True)
    fps=models.CharField(max_length=200,null=True,blank = True)
    is_streaming = models.CharField(max_length=200,null=True,blank = True) 
    url = models.CharField(max_length=200,null=True,blank = True)
    last_update = models.CharField(max_length=200,null=True,blank = True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return str(self.camera_name)



class SystemHeartBeat(models.Model):
    heartbeat_id= models.UUIDField(primary_key=True,editable=False,default = uuid.uuid4)
    hostname = models.CharField(max_length=200,null=True,blank = True)
    os=models.CharField(max_length=200,null=True,blank = True)
    system_uptime_seconds=models.CharField(max_length=200,null=True,blank = True)
    service_uptime_seconds=models.CharField(max_length=200,null=True,blank = True)
    license_valid =models.CharField(max_length=200,null=True,blank = True)
    recording_enabled = models.CharField(max_length=200,null=True,blank = True)
    cpu_cores=models.CharField(max_length=200,null=True,blank = True)
    cpu_last_update=models.CharField(max_length=200,null=True,blank = True)
    mac_id =models.CharField(max_length=200,null=True,blank = True)
    ip_address =models.CharField(max_length=200,null=True,blank = True)
    cpu_usage_percent=models.CharField(max_length=200,null=True,blank = True)
    disk_quota_total_bytes=models.CharField(max_length=200,null=True,blank = True)
    disk_quota_consumed_bytes=models.CharField(max_length=200,null=True,blank = True)
    memory_consumed_bytes=models.CharField(max_length=200,null=True,blank = True)
    memory_total_bytes=models.CharField(max_length=200,null=True,blank = True)
    camera_hearbeat = models.ManyToManyField(CameraHeartBeat,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return str(self.hostname)

		
		
class ANPRLPDetails(models.Model):
    uid=models.CharField(max_length=200,null=True,blank = True)
    ip_address=models.CharField(max_length=200,null=True,blank = True)
    device_name = models.CharField(max_length=200,null=True,blank = True)
    lpu_password=models.CharField(max_length=200,default="NA",null=True,blank = True)
    location=models.CharField(max_length=200,default="NA",null=True,blank = True)
    https_port=models.CharField(max_length=200,default="NA",null=True,blank = True)
    ssh_port=models.CharField(max_length=200,default="NA",null=True,blank = True)
    registered_status=models.CharField(max_length=200,default="NA",null=True,blank = True)
    interface_id=models.CharField(max_length=200,default="NA",null=True,blank = True)
    status=models.CharField(max_length=200,null=True,blank = True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
	    return self.ip_address


	
class ANPRMaster(models.Model):
    anpr_id=models.CharField(max_length=200,null=True,blank = True)
    device_id=models.CharField(max_length=200,null=True,blank = True)
    anpr_camera_name=models.CharField(max_length=200,null=True,blank = True)
    app_key=models.CharField(max_length=200,null=True,blank = True)
    anpr_device_id=models.CharField(max_length=200,null=True,blank = True)
    camera_ip_address=models.CharField(max_length=200,null=True,blank = True)
    cam_port=models.CharField(max_length=200,null=True,default="NA",blank = True)
    cam_user_name=models.CharField(max_length=200,default="NA",null=True,blank = True)
    cam_password=models.CharField(max_length=200,default="NA",null=True,blank = True)
    cam_registered_status=models.CharField(max_length=200,default="REGISTERED",null=True,blank = True)
    status=models.CharField(max_length=200,null=True,blank = True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.anpr_camera_name

	
class Transaction(models.Model):
    strAccessStatus=models.CharField(max_length=200,default="NA",null=True,blank = True)
    AppKey=models.CharField(max_length=200,default="NA",null=True,blank = True)
    strCamName=models.CharField(max_length=200,default="NA",null=True,blank = True)
    cam_name=models.CharField(max_length=200,default="NA",null=True,blank = True)
    Confidence=models.CharField(max_length=200,default="NA",null=True,blank = True)
    ContactNum=models.CharField(max_length=200,default="NA",null=True,blank = True)
    strCaptureDatetime=models.CharField(max_length=200,default="NA",null=True,blank = True)
    strDirection=models.CharField(max_length=200,default="NA",null=True,blank = True)
    DriverImagePath=models.CharField(max_length=200,default="NA",null=True,blank = True)
    DriverImage=models.CharField(max_length=200,default="NA",null=True,blank = True)
    byteLpImg=models.TextField(null=True,blank = True)
    No=models.CharField(max_length=200,default="NA",null=True,blank = True)
    strAnprImgPath=models.CharField(max_length=200,default="NA",null=True,blank = True)
    strRecNum=models.CharField(max_length=200,default="NA",null=True,blank = True)
    strDevID=models.CharField(max_length=200,default="NA",null=True,blank = True)
    RegName=models.CharField(max_length=200,default="NA",null=True,blank = True)
    U_Id=models.CharField(max_length=200,default="NA",null=True,blank = True)
    Validated=models.CharField(max_length=200,default="NA",null=True,blank = True)
    extractedLPImage=models.CharField(max_length=200,default="NA",null=True,blank = True)
    VehicleMake=models.CharField(max_length=200,default="NA",null=True,blank = True)
    strVehicleType=models.CharField(max_length=200,default="NA",null=True,blank = True)
    
    def __str__(self):
        return self.strRecNum





class ANPRVehicleRegisterationDetail(models.Model):
    license_number=models.CharField(max_length=200,null=True,blank = True)
    vehicle_type=models.CharField(max_length=200,null=True,blank = True)
    vehicle_colour=models.CharField(max_length=200,null=True,blank = True)
    number_plate_ocr = models.TextField(null=True,blank = True)
    frame_url = models.CharField(max_length=200,null=True,blank = True)
    vehicle_prevelege=models.CharField(max_length=200,default="NA",null=True,blank = True)
    driver_name=models.CharField(max_length=200,default="NA",null=True,blank = True)
    contact_no=models.CharField(max_length=200,null=True,default="NA",blank = True)
    company_name=models.CharField(max_length=200,null=True,blank = True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
	    return self.license_number
	
	
	
class AlprGroup(models.Model):
    group_id = models.UUIDField(primary_key=True,editable=False,default = uuid.uuid4)
    company_id = models.CharField(max_length=200,null=True,blank = True)
    agent_id = models.CharField(max_length=200,null=True,blank = True)
    camera_id =models.CharField(max_length=200,null=True,blank = True)
    best_plate_number = models.CharField(max_length=200,null=True,blank = True)
    best_confidence = models.CharField(max_length=200,null=True,blank = True)
    best_region = models.CharField(max_length=200,null=True,blank = True)
    best_region_confidence = models.CharField(max_length=200,null=True,blank = True)
    best_image_width = models.CharField(max_length=200,null=True,blank = True)
    best_image_height = models.CharField(max_length=200,null=True,blank = True)
    epoch_start = models.CharField(max_length=200,null=True,blank = True)
    epoch_end = models.CharField(max_length=200,null=True,blank = True)
    frame_start = models.CharField(max_length=200,null=True,blank = True)
    frame_end = models.CharField(max_length=200,null=True,blank = True)
    vehicle_label = models.CharField(max_length=200,null=True,blank=True,default="NA")
    is_parked = models.CharField(max_length=200,null=True,blank = True)
    agent_type = models.CharField(max_length=200,null=True,blank = True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return self.best_plate_number
