from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets
from .models import Transaction, Image, AlprGroup, SystemHeartBeat, CameraHeartBeat, VehicleMaster, ANPRLPDetails, \
    ANPRMaster, ANPRVehicleRegisterationDetail
from .serializer import TransactionSerializer, ImageSerializer, AlprSerializer, SystemHeartBeatSerializer, \
    CameraHeartBeatSerializer, VehicleMasterSerializer, ANPRVehicleRegisterationDetailSerializer, ANPRMasterSerializer, \
    ANPRLPDetailsSerializer
from django.http import JsonResponse
import requests
import traceback
import datetime
import json
import uuid
import socket
import base64
from io import BytesIO
import re
from django.core.files.base import ContentFile
from urllib.request import urlopen
from django_filters.rest_framework import DjangoFilterBackend
import logging
logger = logging.getLogger(__name__)


# {'C4-00-AD-09-9E-66' : '192.168.1.49','host_name':'LPU3'}
# 192.168.1.236:8000/alpr/process/data/
def getIp():
    hostname = socket.gethostname()
    IPAddr = socket.gethostbyname(hostname)
    return IPAddr


def getMacAddress():
    mac_id = ':'.join(['{:02x}'.format((uuid.getnode() >> ele) & 0xff) for ele in range(0, 8 * 6, 8)][::-1])
    return mac_id


# Create your views here.
@csrf_exempt
def getResponse(request):
    try:
        if request.method == 'POST':
            # print("hello")
            # print(request.body)
            data = json.loads(request.body)
            print(data)
            if (data != None or len(data) > 0):
                data_type = data.get("data_type", None)
                if (data_type != None and data_type == "alpr_group"):
                    company_id = data.get("company_id", None)
                    agent_id = data.get("agent_uid", None)
                    best_plate_number = data.get("best_plate_number", None)
                    best_confidence = data.get("best_confidence", None)
                    best_region = data.get("best_region", None)
                    best_region_confidence = data.get("best_region_confidence", None)
                    best_image_width = data.get("best_image_width", None)
                    best_image_height = data.get("best_image_height", None)
                    epoch_start = data.get("epoch_start", None)
                    epoch_end = data.get("epoch_end", None)
                    frame_start = data.get("frame_start", None)
                    frame_end = data.get("frame_end", None)
                    is_parked = data.get("is_parked", None)
                    agent_type = data.get("agent_type", None)
                    direction = data.get("travel_direction", None)
                    direction_value = getEntryAndExit(direction)
                    camera_id = data.get("camera_id", None)
                    user_data = data.get("user_data", None)
                    user_data = eval(user_data)
                    ip = ""
                    mac_id = ""
                    host_name = ""
                    if (user_data):
                        ip = list(user_data.values())[0].strip()
                        mac_id = list(user_data.keys())[0].strip()
                        host_name = list(user_data.values())[1].strip()
                    vehicle = data.get("vehicle", None)
                    body_type = vehicle.get("body_type", None)
                    vehicle_type = body_type[0].get('name', "NA")
                    color = vehicle.get("color", None)
                    vehicle_color = color[0].get("name", "NA")
                    ocr = data.get("best_plate", None)
                    ocr = ocr.get('plate_crop_jpeg', None)
                    frame_url = f"http://{ip}:8355/img/{data.get('best_uuid', None)}.jpg"
                    url, id = saveFrameImage(frame_url, data.get('best_uuid', None))

                    veh = storeVehicleRegisterationEntry(best_plate_number, vehicle_type, vehicle_color, ocr, url, "NA")
                    print(veh)
                    # alprGroup=AlprGroup(vehicle_label=label,company_id=company_id,agent_id=agent_id,best_plate_number=best_plate_number,best_confidence=best_confidence,best_region=best_region,best_region_confidence=best_region_confidence,best_image_width=best_image_width,best_image_height=best_image_height,epoch_start=epoch_start,epoch_end=epoch_end,frame_start=frame_start,frame_end=frame_end,is_parked=is_parked,camera_id=camera_id)
                    # alprGroup.save()
                    cam = ANPRMaster.objects.filter(anpr_device_id=camera_id)
                    if (len(cam) == 0):
                        cam_name = "NA"
                    else:
                        cam_name = cam[0].anpr_camera_name
                    header = {"Content-type": "application/json"}
                    # mac_id=':'.join(['{:02x}'.format((uuid.getnode() >> ele) & 0xff) for ele in range(0,8*6,8)][::-1])

                    request = {"strAccessStatus": veh, "AppKey": str(agent_id), "strCamName": cam_name,
                               "Confidence": str(round(best_confidence)), "ContactNum": "abc",
                               "strCaptureDatetime": str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
                               "strDirection": direction_value, "DriverImagePath": "", "DriverImage": None,
                               "byteLpImg": ocr, "No": 0, "strAnprImgPath": str(url),
                               "strRecNum": str(best_plate_number), "strDevID": mac_id, "RegName": "abc", "U_Id": 0,
                               "Validated": "abc", "extractedLPImage": "", "VehicleMake": "abc",
                               "strVehicleType": vehicle_type}

                    print(request)
                    r = requests.post("http://192.168.1.2:4532/ANPRService/ConsumeAnprAlert", data=json.dumps(request),
                                      headers=header)
                    transaction = Transaction(strAccessStatus=veh, AppKey=str(agent_id), strCamName=cam_name,
                                              Confidence=str(round(best_confidence)), ContactNum="abc",
                                              strCaptureDatetime=str(
                                                  datetime.datetime.now().strftime("%Y-%m-%d %H=%M=%S")),
                                              strDirection=direction_value, DriverImagePath="", DriverImage=None,
                                              byteLpImg=ocr, No=0, strAnprImgPath=str(url),
                                              strRecNum=str(best_plate_number), strDevID=mac_id, RegName="abc",
                                              U_Id="0", Validated="abc", extractedLPImage="", VehicleMake="abc",
                                              strVehicleType=vehicle_type)
                    transaction.save()
                    ANPRMasterEntry(None, None, None, mac_id, camera_id, None, None, True)
                    ANPRLPDetailsEntry(mac_id, ip, host_name, None, None, None, None, None, "ON")
                    return JsonResponse({'status': 'success'}, status=200)
                elif (data_type != None and data_type == "heartbeat"):
                    hostname = data.get("agent_hostname", None)
                    os = data.get("os", None)
                    system_uptime_seconds = data.get("system_uptime_seconds", None)
                    service_uptime_seconds = data.get("service_uptime_seconds", None)
                    license_valid = data.get("license_valid", None)
                    recording_enabled = data.get("recording_enabled", None)
                    cpu_cores = data.get("cpu_cores", None)
                    cpu_last_update = data.get("cpu_last_update", None)
                    cpu_usage_percent = data.get("cpu_usage_percent", None)
                    disk_quota_total_bytes = data.get("disk_quota_total_bytes", None)
                    disk_quota_consumed_bytes = data.get("disk_quota_consumed_bytes", None)
                    memory_consumed_bytes = data.get("memory_consumed_bytes", None)
                    memory_total_bytes = data.get("memory_total_bytes", None)
                    camera_hearbeat = data.get("video_streams", None)
                    # heartbeat=SystemHeartBeat(ip_address=getIp(),mac_id = getMacAddress(),hostname=hostname,os=os,system_uptime_seconds=system_uptime_seconds,service_uptime_seconds=service_uptime_seconds,license_valid=license_valid,recording_enabled=recording_enabled,cpu_cores=cpu_cores,cpu_last_update=cpu_last_update,cpu_usage_percent=cpu_usage_percent,disk_quota_total_bytes=disk_quota_total_bytes,disk_quota_consumed_bytes=disk_quota_consumed_bytes,memory_total_bytes=memory_total_bytes,memory_consumed_bytes=memory_consumed_bytes)
                    # heartbeat.save()

                    ANPRLPDetailsEntry(None, None, hostname, None, None, None, None, None, "ON")
                    for form in camera_hearbeat:
                        camera_id = form.get("camera_id", None)
                        camera_name = form.get("camera_name", None)
                        fps = form.get("fps", None)
                        is_streaming = form.get("is_streaming", None)
                        url = form.get("url", None)
                        last_update = form.get("last_update", None)
                        # cameraHeartBeat=CameraHeartBeat(camera_id=camera_id,camera_name=camera_name,fps=fps,is_streaming=is_streaming,url=url,last_update=last_update)
                        # cameraHeartBeat.save()
                        ip = re.findall(r'[0-9]+(?:\.[0-9]+){3}', url)
                        if (len(ip) == 0):
                            ip = "NA"
                        else:
                            ip = ip[0]
                        ANPRMasterEntry(None, None, camera_name, None, camera_id, None, ip, is_streaming)
                        # heartbeat.camera_hearbeat.add(cameraHeartBeat)
                    return JsonResponse({'status': 'success'}, status=200)

                # AlprGroup.objects.filter(best_plate_number__contains=best_plate_number)

            return JsonResponse({"status": "failure"}, status=200)
    except BaseException as e:
        traceback.print_exc()
        logging.info("Exception has occured", exc_info=1)


        return JsonResponse({'status': 'failure'}, status=400)


class AlprViewSet(viewsets.ModelViewSet):
    serializer_class = AlprSerializer
    queryset = AlprGroup.objects.all()


class SystemHeartBeatViewSet(viewsets.ModelViewSet):
    serializer_class = SystemHeartBeatSerializer
    queryset = SystemHeartBeat.objects.all()


class VehicleMasterViewSet(viewsets.ModelViewSet):
    serializer_class = VehicleMasterSerializer
    queryset = VehicleMaster.objects.all()


class ANPRVehicleRegisterationDetailViewSet(viewsets.ModelViewSet):
    serializer_class = ANPRVehicleRegisterationDetailSerializer
    queryset = ANPRVehicleRegisterationDetail.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = ('license_number',)


    def create(self,request):
        data = json.loads(request.body)
        try:
            license_number= data.get("license_number")
            veh=ANPRVehicleRegisterationDetail.objects.get(license_number=license_number)
            veh.vehicle_prevelege = data.get("vehicle_prevelege","NA")
            veh.driver_name=data.get("driver_name","NA")
            veh.contact_no=data.get("contact_no","NA")
            veh.company_name=data.get("company_name","NA")
            veh.save()
            return JsonResponse({"status": "updated"}, status=200)

        except BaseException as e:
            logging.info("Exception has occured", exc_info=1)
            serializerData=ANPRVehicleRegisterationDetailSerializer(data=request.data)
            if(serializerData.is_valid()):
                serializerData.save()
                return JsonResponse({"status": "created"}, status=200)




class ANPRMasterViewSet(viewsets.ModelViewSet):
    serializer_class = ANPRMasterSerializer
    queryset = ANPRMaster.objects.all()


class ANPRLPDetailsViewSet(viewsets.ModelViewSet):
    serializer_class = ANPRLPDetailsSerializer
    queryset = ANPRLPDetails.objects.all()


class ImageViewSet(viewsets.ModelViewSet):
    serializer_class = ImageSerializer
    queryset = Image.objects.all()


class TransactionViewSet(viewsets.ModelViewSet):
    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()


def getEntryAndExit(travel_direction):
    travel_direction = float(travel_direction)
    value = ""
    if (travel_direction < 100 or travel_direction > 300):
        value = "Exit"
    elif (travel_direction > 100 or travel_direction < 300):
        value = "Entry"

    return value


def ANPRMasterEntry(anpr_id, device_id, anpr_camera_name, app_key, anpr_device_id, cam_user_name, camera_ip_address,
                    status):
    try:
        cam = ANPRMaster.objects.get(anpr_device_id=anpr_device_id)
        if (app_key != None and app_key != ""):
            if (cam.app_key == None or cam.app_key == ""):
                cam.app_key = app_key
                cam.status = status
                cam.save()
            else:
                cam.status = status
                cam.save()
        else:
            cam.status = status
            cam.camera_ip_address = camera_ip_address
            cam.anpr_camera_name = anpr_camera_name
            cam.status = status
            cam.save()




    except BaseException as e:
        logging.info("Exception has occured", exc_info=1)
        master = ANPRMaster(anpr_id=anpr_id, device_id=device_id, cam_user_name=cam_user_name,
                            anpr_camera_name=anpr_camera_name, app_key=app_key, anpr_device_id=anpr_device_id,
                            camera_ip_address=camera_ip_address, status=status)
        master.save()


def ANPRLPDetailsEntry(uid, ip_address, device_name, lpu_password, location, https_port, ssh_port, registered_status,
                       status):
    try:
        if (uid != "" and uid != None and ip_address != "" and ip_address != None):
            lp_details = ANPRLPDetails.objects.get(device_name=device_name)
            if ((lp_details.uid == None and lp_details.ip_address == None) or (
                    lp_details.uid == "" and lp_details.ip_address == "")):
                lp_details.uid = uid
                lp_details.ip_address = ip_address
                lp_details.status = "ON"
                lp_details.save()
            lp_details.status = "ON"
            lp_details.save()
        else:
            lp_details = ANPRLPDetails.objects.get(device_name=device_name)
            lp_details.status = "ON"
            lp_details.save()

    except BaseException as e:

        logging.info("Exception has occured", exc_info=1)
        lp_details = ANPRLPDetails(uid=uid, ip_address=ip_address, device_name=device_name, status=status)
        lp_details.save()


def storeVehicleRegisterationEntry(license_number, vehicle_type, vehicle_colour, number_plate_ocr, frame_url,
                                   vehicle_prevelege):
    vehicle = ANPRVehicleRegisterationDetail.objects.filter(license_number__contains=license_number)
    prevelege = ""
    if (len(vehicle) == 1):
        prevelege = vehicle[0].vehicle_prevelege
    elif (len(vehicle) == 0):
        reg = ANPRVehicleRegisterationDetail(license_number=license_number, vehicle_type=vehicle_type,
                                             vehicle_colour=vehicle_colour, number_plate_ocr=number_plate_ocr,
                                             frame_url=frame_url, vehicle_prevelege=vehicle_prevelege)
        reg.save()
        prevelege = "NA"
    else:
        try:
            vehicle = ANPRVehicleRegisterationDetail.objects.get(license_number=license_number)
            prevelege = vehicle.prevelege
        except BaseException as e:
            logging.info("Exception has occured", exc_info=1)

            # reg=ANPRVehicleRegisterationDetail(license_number=license_number,vehicle_type=vehicle_type,vehicle_colour=vehicle_colour,number_plate_ocr=number_plate_ocr,frame_url=frame_url,vehicle_prevelege=prevelege)
            # reg.save()
            # prevelege="NA"
    return prevelege


# def saveOCRImage(img,filename):
#     data=base64.b64decode(img)
#     # with open(filename+".jpg","wb") as f:
#     #     f.write(data)
#     fp = BytesIO()
#     fp.write(data)
#     i=Image.objects.create()
#     i.image = ImageFile(fp)
#     i.save()


def saveFrameImage(url, filename):
    try:
        input_file = BytesIO(urlopen(url).read())  # img_url is simply the image URL
        person = Image.objects.create()
        person.image.save(filename + ".jpg", ContentFile(input_file.getvalue()), save=False)
        person.save()
        img = Image.objects.get(image__contains=filename + ".jpg")
        return img.image.url, img.id
    except BaseException as e:
        logging.info("Exception has occured", exc_info=1)
        return "NA", "NA"



