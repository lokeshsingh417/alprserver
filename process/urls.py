
from django.urls import path,include
from .views import TransactionViewSet,ImageViewSet,getResponse,AlprViewSet,SystemHeartBeatViewSet,VehicleMasterViewSet,ANPRVehicleRegisterationDetailViewSet,ANPRMasterViewSet,ANPRLPDetailsViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
# router.register('result', AlprViewSet, basename='result')
# router.register('heartbeat',SystemHeartBeatViewSet,basename="heartbeat")
# router.register('master',VehicleMasterViewSet,basename="vehicle-master")

router.register('images', ImageViewSet, basename='images')

router.register('vehicleRegisteration', ANPRVehicleRegisterationDetailViewSet, basename='vehicleRegisteration')
router.register('anprMaster',ANPRMasterViewSet,basename="anprMaster")
router.register('lpDetails',ANPRLPDetailsViewSet,basename="lpDetails-master")
router.register('transaction',TransactionViewSet,basename="transaction")
urlpatterns = [
    path('data/', getResponse,name="getResponse"),
]

urlpatterns+= router.urls


    